#!/usr/bin/python3

import time,sys,traceback,logging,daemonize
from PLCManagerControl import checkContainerStatus,StartPLC,StopPLC
from DBManage import setContainerStatus,getRunningContainers,setContainerIdentifier,getPendingStartCommands,getPendingStopCommands

name = "plc_managerd"
pid = "/var/run/"+name+".pid"

def work():

	#Create logger
	logging.basicConfig(filename='/var/log/plc_managerd.log', level=logging.INFO)

	#Say it's started
	logging.info("Plc Manager started working")

	while 1:
		"""
			Reset corrupted status
		"""
		logging.debug("Checking corrupted PLCs")
		try:
			containers = getRunningContainers()
		except:
			logging.error("Unable to get container list")
			containers = []

		for id,container in containers:

			#Container does not exist => 0
			#Container not started => 0 and remove container
			#Container not responding => 3
			try:
				status = checkContainerStatus(container)
			except:
				status = 1
				logging.error("Unable to check a container")
			#Status is not good => set status
			if status != 1: 
				logging.warning("Invalid status of container: ",container,status)
				try:
					setContainerStatus(id,status)
				except:
					status = 1
					logging.error("Unable to set container status")

		"""
			Execute start commands
		"""
		#Start commands
		logging.debug("Start commands")
		try:
			containers = getPendingStartCommands()
		except:
			containers = []
			logging.error("Unable to set start container list")
		#Execute start_commands
		for id,program in containers:
			logging.info("Starting PLC "+str(id))
			try:
				identifier = StartPLC(program)
				setContainerIdentifier(id,identifier)
				setContainerStatus(id,1)
			except:
				logging.error("Unable to start a container")

		"""
			Execute stop commands
		"""
		logging.debug("Stop commands")
		#Execute stop commands
		try:
			containers = getPendingStopCommands()
		except:
			containers = []
			logging.error("Unable to set stop container list")
		#Execute start_commands
		for id,identifier in containers:
			try:
				logging.info("Stopping PLC "+str(id))
				identifier = StopPLC(identifier)
				setContainerStatus(id,0)
			except:
				logging.error("Unable to stop a container")

	        #Sleep
		logging.debug("Going to sleep")
		time.sleep(5)

daemon = daemonize.Daemonize(app=name, pid=pid, action=work)
daemon.start()
