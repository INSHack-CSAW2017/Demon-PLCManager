import docker, tarfile, time, sys
from io import BytesIO


def StartPLC(Program):
    client=docker.from_env()

    #Create new container
    bc = client.containers.create("csaw/plc")

    #Copy file into container
    pr = bc.put_archive(path='/plc/',data=Program)

    #Start container
    bc.start()

    return bc.id

def StopPLC(id):
    client=docker.from_env()

    #Get container from list
    container = client.containers.list(True,filters={'id': id})

    #If container is already removed
    if len(container) == 0:return
    container = container[0]

    #Try to stop container
    try:
        container.stop()
    except:
        pass

    container.remove()

def checkContainerStatus(id):
    client=docker.from_env()

    #Get container from list
    container = client.containers.list(True,filters={'id': id})

    #If container is already removed
    if len(container) == 0:
        return 0
    container = container[0]

    #Check if container is still running
    container_running = client.containers.list(True,filters={'id': id,'status':'running'})
    if len(container_running) == 0:
        container.remove()
        return 0

    #Check if openplc is still running
    plist = ""
    for process in container.top()['Processes']:
        plist += process[7]

    #Check specific process
    if "/plc/openplc" not in plist:return 3
    if "nginx" not in plist:return 3
    if "uwsgi" not in plist:return 3

    return 1

if __name__ == '__main__':
    argv = sys.argv

    if len(argv) != 2:
        print("Usage: python3 PLCBuilderControl.py program.tar")
        sys.exit(-1)

    #Get file content
    content = ""
    with open(argv[1],'rb') as f: content = f.read()

    #Execute Build function
    id = StartPLC(content)
    print("Status",checkContainerStatus(id))
    StopPLC(id)

    print(id)
