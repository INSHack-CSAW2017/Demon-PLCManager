import MySQLdb

def Connect():
    conn = MySQLdb.connect(
        host="localhost",
        user="demon_plc_manager",
        passwd="",
        db="plc_manager"
    )

    return conn
