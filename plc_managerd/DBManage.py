from DBConnect import Connect

"""
	Query the database for running containers
	Return a list of identifiers
"""
def getRunningContainers():
	connection = Connect()
	cursor = connection.cursor()

	cursor.execute(
		"SELECT plc_id,identifier "
		"FROM PLC_PlcManagerDemon "
		"WHERE status=1 "
		"AND command=1 "
	)

	identifiers = []
	results = cursor.fetchall()
	for result in results: identifiers.append(result)

	cursor.close()
	connection.close()

	return identifiers

"""
	Set container status from its id
	returns nothing
"""
def setContainerStatus(id,status):
	connection = Connect()
	cursor = connection.cursor()
	
	cursor.execute(
		"UPDATE PLC_PlcManagerDemon SET status=%s WHERE plc_id=%s",
		(status,id)
	)

	connection.commit()
	cursor.close()
	connection.close()

"""
	Get the list of starting commands
	Return a list of tuples containing id and program to be launched

"""
def getPendingStartCommands():
	connection = Connect()
	cursor = connection.cursor()

	cursor.execute(
		" SELECT PLC_PlcManagerDemon.plc_id,Programs_PlcManagerDemon.tar_program "
		" FROM PLC_PlcManagerDemon,Programs_PlcManagerDemon "
		" WHERE PLC_PlcManagerDemon.program_id=Programs_PlcManagerDemon.program_id "
		" AND PLC_PlcManagerDemon.command=1 " #Asked to start
		" AND PLC_PlcManagerDemon.status=0 " #No container created
	)

	containers = []
	results = cursor.fetchall()
	for result in results: containers.append(result)

	cursor.close()
	connection.close()

	return containers

"""
	Get the list of stop commands
	Return a list of tuples containing id and program to be launched

"""
def getPendingStopCommands():
	connection = Connect()
	cursor = connection.cursor()

	cursor.execute(
		" SELECT plc_id,identifier "
		" FROM PLC_PlcManagerDemon "
		" WHERE command=0 " #Asked to stop
		" AND status=1 " #Running PLC
		" OR status=2 " #Can be also defective
	)

	containers = []
	results = cursor.fetchall()
	for result in results: containers.append(result)

	cursor.close()
	connection.close()

	return containers


"""
	Set the identifier at start
"""
def setContainerIdentifier(id,identifier):
	connection = Connect()
	cursor = connection.cursor()
	
	cursor.execute(
		"UPDATE PLC_PlcManagerDemon SET identifier=%s WHERE plc_id=%s",
		(identifier,id)
	)

	connection.commit()
	cursor.close()
	connection.close()
