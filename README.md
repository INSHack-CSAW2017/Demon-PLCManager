# Démon Manager

## Install
Vous devez d'abord construire l'image csaw/plcr via le repo:

https://gitlab.com/INSHack-CSAW2017/Docker_PLC    

Et avoir la base de donnée du site de controle de PLC sur un serveur mariadb

https://gitlab.com/INSHack-CSAW2017/Website-PLCControl

    sudo apt-get install python3 python3-pip
    sudo python3 -m pip install --upgrade pip
    sudo pip3 install docker mysqlclient daemonize
    sudo ./install.sh
    sudo service plc_managerd start

## TODO
- Watchdog
- Permissions

## Description
Ce demon regarde dans la base de donnée les différentes commandes (démarrage, arret) puis les execute.
Toutes les 5 secondes, il vérifie l'état de tous les conteneurs, ainsi que les processus a l'interieur.
