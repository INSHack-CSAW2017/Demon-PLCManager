#!/bin/bash

#Install required
apt-get install -y python3 python3-pip mariadb-server libmariadbclient-dev
pip3 install docker mysqlclient daemonize

#Copy executables
cp -R plc_managerd /usr/sbin/

#Move init file
cp init-script/plc_managerd /etc/init.d/plc_managerd
sudo chmod 0755 /etc/init.d/plc_managerd

#Reload daemons
systemctl daemon-reload

#Add at start
sudo update-rc.d plc_managerd defaults

#Start service
sudo service plc_managerd start
